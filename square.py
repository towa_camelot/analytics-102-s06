def calculate_square(number):
    """Calculates the square of a given number."""
    square = number ** 2
    return square
