import math

def calculate_log10(number):
    """Calculates the logarithm base 10 of a given number."""
    log_value = math.log10(number)
    return log_value

def calculate_log2(number):
    """Calculates the logarithm base 10 of a given number."""
    log_value = math.log2(number)
    return log_value
