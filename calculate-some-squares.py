# Databricks notebook source
from square import calculate_square

# COMMAND ----------

calculate_square(3)

# COMMAND ----------

calculate_square(23)

# COMMAND ----------
calculate_square(800)

# COMMAND ----------
calculate_square(4002)

# COMMAND ----------
calculate_square(123913)
