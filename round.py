def round_decimal(number, decimal_places):
    """Rounds a decimal number to the specified number of decimal places."""
    rounded_value = round(number, decimal_places)
    return rounded_value